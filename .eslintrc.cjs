/** @type {import("eslint").ESLint.ConfigData} */
module.exports = {
  env: {
    node: true,
  },
  extends: [
    "eslint:recommended",
    // 'plugin:vue/vue3-recommended', // Use this if you are using Vue.js 3.x
    "plugin:vue/recommended", // Use this if you are using Vue.js 2.x.
    "prettier",
  ],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
    "vue/require-default-prop": "off",
    "vue/multi-word-component-names": [
      "error",
      {
        ignores: ["Fragment"],
      },
    ],
  },
  ignorePatterns: ["src/pages/*", "src/layouts/*", "*.d.ts"],
};
