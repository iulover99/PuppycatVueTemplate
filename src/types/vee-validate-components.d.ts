// vee-validate@3 components types. More info about document here: https://vee-validate.logaretm.com/v3/guide/basics.html

import { VNode } from "vue";

interface ObserverField {
  id: string;
  name: string;
  failedRules: Record<string, string>;
  pristine: boolean;
  dirty: boolean;
  touched: boolean;
  untouched: boolean;
  valid: boolean;
  invalid: boolean;
  pending: boolean;
  validated: boolean;
  changed: boolean;
  passed: boolean;
  failed: boolean;
}

export interface ValidationFlags {
  untouched: boolean;
  touched: boolean;
  dirty: boolean;
  pristine: boolean;
  valid: boolean;
  invalid: boolean;
  passed: boolean;
  failed: boolean;
  validated: boolean;
  pending: boolean;
  required: boolean;
  changed: boolean;
  [x: string]: boolean | undefined;
}

export interface ValidationObserverProps {
  tag?: string
  slim?: boolean
  disabled?: boolean
  vid?: string
}

export interface ValidationProviderProps {
  bails?: boolean
  customMessages?: {[k: string]: string }
  debounce?: number
  detectInput?: boolean
  disabled?: boolean
  immediate?: boolean
  mode?: string
  name?: string
  rules: string
  skipIfEmpty?: boolean
  tag?: string
  vid?: string
}

export interface ValidationObserverSlotArgument {
  dirty: boolean
  pristine: boolean
  valid: boolean
  invalid: boolean
  pending: boolean
  touched: boolean
  untouched: boolean
  passed: boolean
  failed: boolean
  errors: { [x: string]: string[] }
  validate: () => Promise<boolean>
  validateWithInfo: () => Promise<{ isValid: boolean, errors: Record<string, string>, flags: ValidationFlags, fields: Record<string, ObserverField> }>
  handleSubmit: (cb: Function) => Promise<void>
  reset: () => void
}

export interface ValidationProviderSlotArgument {
  errors: string[]
  failedRules: { [x: string]: string }
  aria: { [x: string]: string }
  classes: { [x: string]: boolean }
  validate: (e) => Promise
  reset: () => void
  valid: boolean | undefined
  invalid: boolean | undefined
  untouched: boolean
  touched: boolean
  dirty: boolean
  pristine: boolean
  passed: boolean
  failed: boolean
  validated: boolean
  pending: boolean
  required: boolean
  changed: boolean
}

export interface ValidationObserverMethods {
  validate: (args: { silent: boolean }) => Promise<boolean>
  reset: () => void
  setErrors: (args: Record<string, string | string[]>) => void
}

export interface ValidationProviderMethods {
  validate: (args: any) => Promise<unknown>
  validateSilent: () => Promise<unknown>
  applyResult: (args: any) => void
  reset: () => void
  setFlags: (args: Object) => void
  setErrors: string[]
  syncValue: (args: any) => void
}

/**
 * {@link https://vee-validate.logaretm.com/v3/api/validation-observer.html}
 * Types for the ValidationObserver component
 */
export declare const ValidationObserver: new () => {
  $props: ValidationObserverProps,
  $scopedSlots: {
    default?: ({}: ValidationObserverSlotArgument) => VNode[] | undefined
  },
}

/**
 * {@link https://vee-validate.logaretm.com/v3/api/validation-provider.html}
 * Types for the ValidationProvider component
 */
export declare const ValidationProvider: new () => {
  $props: ValidationProviderProps,
  $scopedSlots: {
    default?: ({
    }: ValidationProviderSlotArgument) => VNode[] | undefined
  },
}