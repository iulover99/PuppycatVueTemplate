const PROXY_PREFIX = '/api/something/'

const EXAMPLE_API_GENERATED = (/**@type {string}*/prefix) => ({
  GET: prefix + 'example',
  GET_ONE: (exampleID) => prefix + `example/${exampleID}`,
  POST_NEW: prefix + 'example',
  PUT_ONE: (exampleID) => prefix + `example/${exampleID}`,
  PATCH_ONE: (exampleID) => prefix + `example/${exampleID}`,
  DELETE_ONE: (exampleID) => prefix + `example/${exampleID}`
})


export const { EXAMPLE_API } = {
  EXAMPLE_API: EXAMPLE_API_GENERATED(PROXY_PREFIX)
}