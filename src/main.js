import "./style.css";
import Vue from "vue";
import App from "./App.vue";
import routes from "~pages";
import { setupLayouts } from 'virtual:generated-layouts'
import { ValidationProvider, ValidationObserver } from 'vee-validate';

//Only use in vue 2, remove the use and modify if update to v3
import { createRouter } from "vue2-helpers/vue-router";
import frag, { Fragment } from "vue-frag";

//* Vue use register

//* Vue global components register
Vue.component("Fragment", Fragment);
Vue.component("ValidationObserver", ValidationObserver)
Vue.component("ValidationProvider", ValidationProvider)

//* Vue global directive register
Vue.directive("frag", frag);

//* Main application installed
const router = createRouter({
  routes: setupLayouts(routes),
});

// Vue2 only
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
