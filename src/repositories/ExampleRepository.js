import { EXAMPLE_API } from "@/constants/apiRoute";

const { GET, GET_ONE, POST_NEW, PUT_ONE, PATCH_ONE, DELETE_ONE } = EXAMPLE_API

/**
 *
 * @param {import("axios").AxiosInstance} axios the current axios instance
 * @returns {IExampleRepository}
 */
const ExampleRepositoryBuilder = (axios) => ({
  get: (params) => axios.get(GET, { params }),
  getOne: (id, params) => axios.get(GET_ONE(id), { params }),
  post: (payload, params) => axios.post(POST_NEW, payload, { params }),
  put: (id, payload, params) => axios.post(PUT_ONE(id), payload, { params }),
  patch: (id, payload, params) => axios.post(PATCH_ONE(id), payload, { params }),
  delete: (id, params) => axios.delete(DELETE_ONE(id), { params })
})

export default ExampleRepositoryBuilder

/**
 * @typedef {{
 *  get: (params?) => Promise<*>,
 *  getOne:(id:string|number, params?) => Promise<*>,
 *  post:(payload, params?) => Promise<*>,
 *  put:(id:string|number, payload, params?) => Promise<*>,
 *  patch:(id:string|number, payload, params?) => Promise<*>,
 *  delete:(id:string|number, params?) => Promise<*>,
 * }} IExampleRepository
 */