import ExampleRepositoryBuilder from "./ExampleRepository";

/**
 * Combine all repository into one big object to use in plugin
 * @param {import("axios").AxiosInstance} axios the current axios instance
 * @returns {IRepositories}
 */
const repositoryBuilder = axios => ({
  example: ExampleRepositoryBuilder(axios)
})

export { repositoryBuilder }

/**
 * @typedef {{
 *  example: import("./ExampleRepository").IExampleRepository
 * }} IRepositories
 */