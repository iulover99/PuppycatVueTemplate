import { extend } from "vee-validate";

// Add a new custom rules here
extend('secret', {
  validate: value => value === 'example',
  message: 'This is not the magic word'
});