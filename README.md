# Puppycat Vue2 Template

> Template dành cho người thích Vue 3 nhưng lại bị dí phải dùng Vue2 nhưng mà vue-cli thì lỗi thời xỉu UwU.

<p align="center">
  <img src="https://i.giphy.com/media/Q4SAh49zZ1moM/giphy.webp">
</p>

## Sử dụng template

Hãy chắc chắn mình đã cài đặt `degit` và sử dụng `pnpm` để dễ dàng copy và sử dụng template nhanh và tốt nhất. Nếu chưa cài đặt `degit` hãy chạy câu lệnh sau:

```bash
npm install -g degit
```

Chắc chắn mình đang có nodejs > 16.17 trong máy và sử dụng `corepack` để cài đặt `pnpm`. Cần kích hoạt corepack thông qua câu lệnh:

```bash
corepack enable
```

Cài đặt `pnpm` thông qua câu lệnh:

```bash
corepack prepare pnpm@latest --activate
```

Sau khi có `degit` trong máy, đơn giản lấy template về và sử dụng ngay bằng:

```bash
degit git@codeberg.org:iulover99/PuppycatVue2Template.git
```

Chạy thử template ngay:

```bash
cd PuppycatVue2Template
pnpm i
pnpm dev
```

## Tính năng nổi bật

- ⚡pnpm, Vite 3, ESBuild - Tốc độ build và thời gian phát triển được rút ngắn luôn được đặt lên hàng đầu
- 📂Thiết lập route theo tên file
- 📦Components tự động import
- 📒Hỗ trợ layout
- 🎨Tailwind CSS, SASS - Thích viết CSS kiểu gì cũng chơi
- 🍃Vue 2.7 - Hỗ trợ Composition API từ trong trứng nước, dễ dàng nâng cấp lên Vue 3 sau này
- 🪚Vee-validate 3 - Tạo form và validate chưa bao giờ dễ dàng hơn thế
- 🪡Vitest - Unit testing giúp fix bug sớm và giảm thiểu chi phí phát sinh về sau
- ⚙️ESlint, Prettier, Stylelint - Duy trì phong cách code đồng nhất xuyên suốt dự án
- ✏️Sử dụng Repository Pattern + Dependency Injection để cài đặt hệ thống gọi API

## Hệ thống plugin tích hợp

> Liệt kê các plugin được sử dụng của template giải thích một vài tuỳ chọn hiện đang sử dụng

### [Plugin Vue 2](https://github.com/vitejs/vite-plugin-vue2)

Plugin hỗ trợ Vue 2.7 chính thức của Vite, hiện giờ đang dùng với cài đặt có sẵn, các tuỳ chọn thêm có thể xem trong được link ở trên.

### [Vite Aliases](https://github.com/Subwaytime/vite-aliases)

Auto biến tất cả các folder nhánh con đầu trong `src` trở thành đường dẫn tắt, hạn chế sử dụng đường dẫn gián tiếp nhiều nhất có thể.

**Ví dụ:**
Kiến trúc file như sau:

```
src
  assets
  components
  pages
  utilities
  ...
```

Thì các đường dẫn liên quan trong `src` có thể thay thế bắt đầu bằng `@/...`. Tương tự ta sẽ có:

- `src/assets/` -> `@assets`
- `src/components/` -> `@components/`
- `src/pages/` -> `@pages/`
- `src/utilities/` -> `@utilities/`

> Notes: Trong VSCode, nhờ có settings `jsconfig.json` và extension [Path Autocomplete](https://marketplace.visualstudio.com/items?itemName=ionutvmi.path-autocomplete) mà ta sẽ nhận được gợi ý path khi gõ, nhưng hiện giờ trong đó mới chỉ có setting cho `src` thôi, mọi người có thể tuỳ ý thêm settings cho các files còn lại tương ứng cho phù hợp để có autocomplete nhé.

### [Vite plugin pages](https://github.com/hannoeru/vite-plugin-pages)

> Hệ thống routing dựa trên file

- Quen thuộc với những ai từng làm Nuxt hoặc Next, không còn phải đau đầu với việc đặt tên router thay thiết kế router để không bị lỗi, giờ tất cả đều trực quan thông qua việc tạo hệ thống folder phù hợp
- Đang được thiết đặt tên route theo phong cách của [NUXT2 framework](https://nuxtjs.org/docs/directory-structure/pages/)

### [unplugin-vue-component](https://github.com/antfu/unplugin-vue-components)

> Tự động import vue component theo lệnh

- Chỉ cần đặt tạo component trong folder cha là `components` thì tất cả các components đều sẽ được định nghĩa và có thể sử dụng ở cấp độ dự án
- Component auto complete nhờ vào Volar
- Tự động import types(Hỗ trợ tốt hơn với Composition API hoặc được định nghĩa bởi một file `.d.ts` riêng)
- __Lưu ý:__ Không giống như Nuxt, thì hệ thống auto import này không tự động thêm prefix dựa theo tên folder nên các component dù cho đặt trong folder thì cũng cần tự ghi prefix để tránh việc trùng tên component sẽ dẫn đến những lỗi không lường trước(Gọi tên component bị trùng dẫn đến dùng sai)

### [Layout](https://github.com/JohnCampionJr/vite-plugin-vue-layouts)

- Hỗ trợ định nghĩa layout và sử dụng layout thông qua folder `layouts`
- Hỗ trợ nhiêu tính năng như thiết đặt transition(vue 3), truyền data từ layout xuống page, cài đặt dữ liệu tĩnh, dữ liệu động.

### [checker](https://github.com/fi3ework/vite-plugin-checker)

> Chạy Typescript, vue-tsc, ESLint, Stylelint qua worker thread

- Đã được cài đặt để chạy eslint và stylelint và fix các lỗi có thể mỗi khi lưu file
- __Lưu ý:__ Nếu VSCode hiện giờ đang sử dụng tính năng autosave, hãy cài đặt thời gian delay autosave lâu hơn một chút, để tránh việc chạy lint mỗi lần gõ, sẽ ảnh hưởng xấu đến hiệu năng code
