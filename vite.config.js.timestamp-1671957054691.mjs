// vite.config.js
import { defineConfig } from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/vite@3.2.5_sass@1.56.2/node_modules/vite/dist/node/index.js";
import vue from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/@vitejs+plugin-vue2@2.1.0_vite@3.2.5+vue@2.7.14/node_modules/@vitejs/plugin-vue2/dist/index.mjs";
import { ViteAliases } from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/vite-aliases@0.9.7_sass@1.56.2/node_modules/vite-aliases/dist/index.js";
import Pages from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/vite-plugin-pages@0.27.1_vite@3.2.5/node_modules/vite-plugin-pages/dist/index.mjs";
import Components from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/unplugin-vue-components@0.22.11_vue@2.7.14/node_modules/unplugin-vue-components/dist/vite.mjs";
import Layouts from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/vite-plugin-vue-layouts@0.7.0_rfsp7pgfwhmqdkbcxothyrk2mq/node_modules/vite-plugin-vue-layouts/dist/index.mjs";
import checker from "file:///C:/Users/MSI-15/Documents/working_directory/PuppycatVue2Template/node_modules/.pnpm/vite-plugin-checker@0.5.1_eslint@8.29.0+vite@3.2.5/node_modules/vite-plugin-checker/dist/esm/main.js";
var vite_config_default = defineConfig({
  plugins: [
    vue(),
    ViteAliases(),
    Pages({
      routeStyle: "nuxt"
    }),
    Components({
      dts: true
    }),
    Layouts({}),
    checker({
      eslint: {
        lintCommand: "eslint --ext .js,.vue --ignore-path .gitignore --fix src"
      },
      stylelint: {
        lintCommand: 'stylelint "src/**/*.{css,vue,scss}" --fix'
      }
    })
  ]
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcuanMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJDOlxcXFxVc2Vyc1xcXFxNU0ktMTVcXFxcRG9jdW1lbnRzXFxcXHdvcmtpbmdfZGlyZWN0b3J5XFxcXFB1cHB5Y2F0VnVlMlRlbXBsYXRlXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCJDOlxcXFxVc2Vyc1xcXFxNU0ktMTVcXFxcRG9jdW1lbnRzXFxcXHdvcmtpbmdfZGlyZWN0b3J5XFxcXFB1cHB5Y2F0VnVlMlRlbXBsYXRlXFxcXHZpdGUuY29uZmlnLmpzXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ltcG9ydF9tZXRhX3VybCA9IFwiZmlsZTovLy9DOi9Vc2Vycy9NU0ktMTUvRG9jdW1lbnRzL3dvcmtpbmdfZGlyZWN0b3J5L1B1cHB5Y2F0VnVlMlRlbXBsYXRlL3ZpdGUuY29uZmlnLmpzXCI7aW1wb3J0IHsgZGVmaW5lQ29uZmlnIH0gZnJvbSBcInZpdGVcIjtcbmltcG9ydCB2dWUgZnJvbSBcIkB2aXRlanMvcGx1Z2luLXZ1ZTJcIjtcbmltcG9ydCB7IFZpdGVBbGlhc2VzIH0gZnJvbSBcInZpdGUtYWxpYXNlc1wiO1xuaW1wb3J0IFBhZ2VzIGZyb20gXCJ2aXRlLXBsdWdpbi1wYWdlc1wiO1xuaW1wb3J0IENvbXBvbmVudHMgZnJvbSBcInVucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3ZpdGVcIjtcbmltcG9ydCBMYXlvdXRzIGZyb20gJ3ZpdGUtcGx1Z2luLXZ1ZS1sYXlvdXRzJztcbmltcG9ydCBjaGVja2VyIGZyb20gJ3ZpdGUtcGx1Z2luLWNoZWNrZXInXG5cbi8vIGh0dHBzOi8vdml0ZWpzLmRldi9jb25maWcvXG5leHBvcnQgZGVmYXVsdCBkZWZpbmVDb25maWcoe1xuICBwbHVnaW5zOiBbXG4gICAgdnVlKCksXG4gICAgVml0ZUFsaWFzZXMoKSxcbiAgICBQYWdlcyh7XG4gICAgICByb3V0ZVN0eWxlOiBcIm51eHRcIixcbiAgICB9KSxcbiAgICBDb21wb25lbnRzKHtcbiAgICAgIGR0czogdHJ1ZVxuICAgIH0pLFxuICAgIExheW91dHMoe30pLFxuICAgIGNoZWNrZXIoe1xuICAgICAgZXNsaW50OiB7XG4gICAgICAgIGxpbnRDb21tYW5kOiAnZXNsaW50IC0tZXh0IC5qcywudnVlIC0taWdub3JlLXBhdGggLmdpdGlnbm9yZSAtLWZpeCBzcmMnLFxuICAgICAgfSxcbiAgICAgIHN0eWxlbGludDoge1xuICAgICAgICBsaW50Q29tbWFuZDogJ3N0eWxlbGludCBcInNyYy8qKi8qLntjc3MsdnVlLHNjc3N9XCIgLS1maXgnLCAvL2xpbnQgLmNzcyAmIC52dWVcbiAgICAgIH0sXG4gICAgIH0pLFxuICBdLFxufSk7XG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQThYLFNBQVMsb0JBQW9CO0FBQzNaLE9BQU8sU0FBUztBQUNoQixTQUFTLG1CQUFtQjtBQUM1QixPQUFPLFdBQVc7QUFDbEIsT0FBTyxnQkFBZ0I7QUFDdkIsT0FBTyxhQUFhO0FBQ3BCLE9BQU8sYUFBYTtBQUdwQixJQUFPLHNCQUFRLGFBQWE7QUFBQSxFQUMxQixTQUFTO0FBQUEsSUFDUCxJQUFJO0FBQUEsSUFDSixZQUFZO0FBQUEsSUFDWixNQUFNO0FBQUEsTUFDSixZQUFZO0FBQUEsSUFDZCxDQUFDO0FBQUEsSUFDRCxXQUFXO0FBQUEsTUFDVCxLQUFLO0FBQUEsSUFDUCxDQUFDO0FBQUEsSUFDRCxRQUFRLENBQUMsQ0FBQztBQUFBLElBQ1YsUUFBUTtBQUFBLE1BQ04sUUFBUTtBQUFBLFFBQ04sYUFBYTtBQUFBLE1BQ2Y7QUFBQSxNQUNBLFdBQVc7QUFBQSxRQUNULGFBQWE7QUFBQSxNQUNmO0FBQUEsSUFDRCxDQUFDO0FBQUEsRUFDSjtBQUNGLENBQUM7IiwKICAibmFtZXMiOiBbXQp9Cg==
