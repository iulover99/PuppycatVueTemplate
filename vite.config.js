import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue2";
import { ViteAliases } from "vite-aliases";
import Pages from "vite-plugin-pages";
import Components from "unplugin-vue-components/vite";
import Layouts from "vite-plugin-vue-layouts";
import checker from "vite-plugin-checker";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    ViteAliases(),
    Pages({
      routeStyle: "nuxt",
    }),
    Components({
      dts: true,
      types: [
        {
          from: "vue-router",
          names: ["RouterLink", "RouterView"],
        },
        {
          from: "@/types/vee-validate-components",
          names: ["ValidationObserver", "ValidationProvider"]
        },
        // {
        //   from: "vee-validate",
        //   names: ["ValidationObserver", "ValidationProvider"]
        // }
      ],
    }),
    Layouts({}),
    checker({
      eslint: {
        lintCommand: "eslint --ext .js,.vue --ignore-path .gitignore --fix src",
      },
      stylelint: {
        lintCommand: "stylelint src/**/*.{css,vue,scss} --fix", //lint .css & .vue
      },
    }),
  ],
});
